import React from 'react'
import { Spin } from 'antd'

const WithSpinner = WrappedComponent => props => {
  return props.loading ? <Spin className="spinner" size="large"/> : <WrappedComponent {...props} />
}
export default WithSpinner
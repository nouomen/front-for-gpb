import React from 'react'
import cookie from 'react-cookies'
import jwt_decode from 'jwt-decode'

const WithToken = WrappedComponent => props => {
  let token = cookie.load('token')
  let username = token ? jwt_decode(token).username : null
  let exp = token ? new Date(token.exp * 1000) : null
  return <WrappedComponent {...props} token={token} username={username} exp={exp}/>
}
export default WithToken
import React, { Component } from 'react'
import { connect } from 'react-redux'
import cookie from 'react-cookies'
import Layout from './components/Layout'

class App extends Component {

  componentDidMount () {
    this.setState({ admin: cookie.load('admin') })
  }

  render () {
    //return <Redirect  to="/login" />;
    return  <Layout view="Некий контент главной"/>
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
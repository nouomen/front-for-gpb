import React, { Component } from 'react'
import { connect } from 'react-redux'
import { onNewPassword } from '../actions/newPassword'
import  NewPasswordForm  from '../components/NewPasswordForm'
import Layout from '../components/Layout'
import AlertSuccess from '../components/AlertSuccess'
import WithToken from '../hoc/WithToken'

const successMessage = 'Пароль успешно изменен! Можете авторизоваться';

class NewPassword extends Component {

  state = {
    password: '',
  }

  onNewPassword = () => this.props.onNewPassword(this.state)
  updatePassword = (password) => this.setState({ password })

  componentDidMount ()
  {
    let confirmation_token = this.props.match.params.hash;
    this.setState({confirmation_token});
  }

  render () {
    let view = <NewPasswordForm loading={this.props.gettingNewPassword}
                            updatePassword={this.updatePassword}
                            onClick={this.onNewPassword} />

    view = this.props.newPassword ? <AlertSuccess message={successMessage}/> : view
    return <Layout view={view} err={this.props.newPasswordError} token={this.props.token}/>
  }
}

const mapStateToProps = (state) => {
  return {
    newPassword: state.newPassword,
    gettingNewPassword: state.gettingNewPassword,
    newPasswordError: state.newPasswordError,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onNewPassword: (data) => dispatch(onNewPassword(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewPassword)
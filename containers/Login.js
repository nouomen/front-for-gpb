import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getJwt } from '../actions/login'
import LoginForm from '../components/LoginForm'
import LogoutForm from '../components/LogoutForm'
import Layout from '../components/Layout'
import WithToken from '../hoc/WithToken'

class Login extends Component {
  state = {
    username: '',
    password: '',
  }

  onLogin = () => {
    this.props.onLogin(this.state)
  }
  updateName = (username) => this.setState({ username })
  updatePassword = (password) => this.setState({ password})

  render () {
    let token = this.props.jwt || this.props.token
    let view
    if (token) {
      view = <LogoutForm username={this.state.username}/>
    } else {
      view = <LoginForm
        loading={this.props.gettingJwt}
        updatePassword={this.updatePassword}
        updateName={this.updateName}
        onClick={this.onLogin}/>
    }

    return <Layout view={view} err={this.props.jwtError} token={token}/>
  }
}

const mapStateToProps = (state) => {
  return {
    jwt: state.jwt,
    jwtError: state.jwtError,
    gettingJwt: state.gettingJwt,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onLogin: (data) => dispatch(getJwt(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithToken(Login))
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layout from '../components/Layout'
import RecoverForm from '../components/RecoverForm'
import { getRecoverLink } from '../actions/recover'
import AlertSuccess from '../components/AlertSuccess'
import WithToken from '../hoc/WithToken'

class Recover extends Component {
  state = {
    username_or_email: '',
  }

  onRecover = () => this.props.onRecover(this.state)

  updateEmail = (username_or_email) => this.setState({ username_or_email })

  render () {

    const props = {
      updateEmail: this.updateEmail,
      onClick: this.onRecover,
      loading: this.props.gettingLink
    }

    let view = this.props.link === null ? <RecoverForm {...props}/> : <AlertSuccess message={this.props.link}/>
    return <Layout view={view} err={this.props.recoverError} />
  }
}

const mapStateToProps = (state) => {
  return {
    link: state.link,
    recoverError: state.recoverError,
    gettingLink: state.gettingLink,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onRecover: (data) => dispatch(getRecoverLink(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Recover)
import React, { Component } from 'react'
import { connect } from 'react-redux'
import RegisterForm from '../components/RegistrationUserForm'
import { makeRegistrationUser } from '../actions/registrationUser'
import Layout from '../components/Layout'
import AlertSuccess from '../components/AlertSuccess'
import WithToken from '../hoc/WithToken'

const successMessage = 'Вы успешно зарегестрированы!';

class RegistrationUser extends Component {

  state = {
    username: '',
    password: '',
    email: '',
    phone: '',
    token: ''
  }

  onRegister = () => this.props.onRegister({
    // токен не должен попасть сюда, он не ожидается api
    username: this.state.username,
    password: this.state.password,
    email: this.state.email,
    phone: this.state.phone
  })

  updateLogin = (username) => this.setState({ username })
  updateEmail = (email) => this.setState({ email })
  updatePassword = (password) => this.setState({ password })
  updatePhone = (phone) => this.setState({ phone })

  componentDidMount() {
    this.setState({ token: this.props.token })
  }

  render () {

    const registerFormProps = {
      loading: this.props.gettingRegistrationUser,
      updateEmail: this.updateEmail,
      updatePassword: this.updatePassword,
      updateLogin: this.updateLogin,
      updatePhone: this.updatePhone,
      onClick: this.onRegister
    }

    let view = this.props.registrationUser === true ? <AlertSuccess message={successMessage}/> : <RegisterForm {...registerFormProps}/>

    return <Layout view={view} token={this.state.token} err={this.props.registrationUserError}/>
  }
}

const mapStateToProps = (state) => {
  return {
    registrationUserError: state.registrationUserError,
    registrationUser: state.registrationUser,
    gettingRegistrationUser: state.gettingRegistrationUser
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onRegister: (data) => dispatch(makeRegistrationUser(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithToken(RegistrationUser))
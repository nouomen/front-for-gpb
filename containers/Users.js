import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layout from '../components/Layout'
import { getUsers } from '../actions/users'
import { Table, Button, Input, Icon } from 'antd'
import WithToken from '../hoc/WithToken'

// юзеров на страницу
const PAGE_SIZE = 3

class Users extends Component {

  state = {
    token: '',
    searchText: '',
  }

  componentDidMount () {
    this.props.getUsers({
      'page': 1,
      'limit': PAGE_SIZE,
      'order': 'asc',
      'order_column': 'id',
    })
    this.setState({
      token: this.props.token,
    })
  }

  /**
   * Поиск филтра
   * @param dataIndex
   * @returns {{filterDropdown: (function({setSelectedKeys: *, selectedKeys?: *, confirm?: *, clearFilters?: *}): *), filterIcon: (function(*): *), onFilter: (function(*, *): boolean), onFilterDropdownVisibleChange: onFilterDropdownVisibleChange, render: (function(*): string)}}
   */
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div className="filterDropdown">
        <Input ref={node => {this.searchInput = node}}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small">
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }}/>
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select())
      }
    },
    render: text => (
      text.toString()
    ),
  })

  handleSearch = (selectedKeys, confirm) => {
    confirm()
    this.setState({ searchText: selectedKeys[0] })
  }

  handleReset = clearFilters => {
    clearFilters()
    this.setState({ searchText: '' })
  }

  /**
   * Колонки грида
   * @type {*[]}
   */
  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      sorter: true,
      type: 'int'
    },
    {
      title: 'username',
      dataIndex: 'username',
      key: 'username',
      ...this.getColumnSearchProps('username'),
      type: 'string'
    },
    {
      title: 'email',
      dataIndex: 'email',
      key: 'email',
      type: 'string',
      ...this.getColumnSearchProps('email'),
    },
    {
      title: 'Активный',
      render: name => name === true ? 'Да' : 'Нет',
      dataIndex: 'enabled',
      key: 'enabled',
      filters: [{ text: 'Да', value: true }, { text: 'Нет', value: false }],
      type: 'bool',
      filterMultiple: false,
    },
    {
      title: 'Дата авторизации',
      dataIndex: 'last_login',
      key: 'last_login',
      type: 'string'
    },
    {
      title: 'Телефон',
      dataIndex: 'phone',
      key: 'phone',
      type: 'string',
      //onFilter: (value, record) => console.log(record),
      filters: [
        {
          text: 'BY',
          value: '375',
        },
        {
          text: 'RU',
          value: '7916',
        },
      ],
    },
  ]

  /**
   * Кастомизация стандартного фильтра грида для отправки в api
   * @param filters
   */
  getCustomFilters = (filters) => {

    let customFilters = {}

    for (let prop in filters) {
      customFilters[prop] = {
        values: filters[prop],
        type: this.columns.find(function (item) {
          return item.key === prop
        }).type
      }
    }
    return customFilters
  }

  /**
   * Изменение грида
   * @param pagination
   * @param filters
   * @param sorter
   */
  onChange = (pagination, filters, sorter) => {

    let data = {
      'filters': this.getCustomFilters(filters),
      'page': pagination.current,
      'limit': pagination.pageSize,
      'order': sorter.order === 'descend' ? 'desc' : 'asc',
      'order_column': sorter.field ? sorter.field : 'id'
    }

    this.props.getUsers(data)
  }

  render () {

    const users = this.props.users
    const dataSource = users ? users.rows : []
    const total = users ? users.total : 0

    const pagination = {
      total: total,
      pageSize: PAGE_SIZE
    }

    let view = <Table
      bordered
      title={() => 'Пользователи площадки'}
      onChange={this.onChange}
      pagination={pagination}
      loading={!!this.props.gettingUsers}
      rowKey={record => record.id}
      dataSource={dataSource}
      columns={this.columns}/>

    return <Layout view={view} token={this.state.token} err={this.props.usersError}/>
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
    gettingUsers: state.gettingUsers,
    usersError: state.usersError
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getUsers: (data) => dispatch(getUsers(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithToken(Users))
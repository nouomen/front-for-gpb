import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layout from '../components/Layout'
import AlertSuccess from '../components/AlertSuccess'
import WithToken from '../hoc/WithToken'
import RegistrationCompanyForm from '../components/RegistrationCompanyForm'
import { getNsiOkvedList, getNsiOkvedTree } from '../actions/nsiOkved'
import { Tree } from 'antd';

const { TreeNode } = Tree;
const successMessage = 'Вы успешно зарегестрированы!';

class RegistrationCompany extends Component {

  state = {
    token: '',
    treeData: [],
    treeNode: null,
    loading: false
  }

  componentDidMount() {
    this.props.getNsiOkvedList({ 'code': null })

    this.setState({
      token: this.props.token,
    })
  }

  onExpand = loadedKeys =>  {
    return console.log('ttt', loadedKeys);
  }

  onLoadData = (treeNode) => {

    this.setState({
      treeNode: treeNode,
    })

    return new Promise(resolve => {
      // обработать
      this.props.getNsiOkvedList({ 'code': treeNode.props.eventKey }, treeNode)
    });
  }

  renderTreeNodes = data => {

    return data.map((item) => {
      if (item.children) {
        return (
          <TreeNode onLoad={this.onLoad} isLeaf={false} title={item.name} key={item.code} dataRef={item}>
            {this.renderTreeNodes(item.children)}
          </TreeNode>
        );
      }

      return <TreeNode onLoad={this.onLoad} isLeaf={false} title={item.name} key={item.code} dataRef={item}/>;
    });
  }

  render () {

    let view;
    if (this.state.treeNode === null && this.props.gettingNsiOkved || !this.props.nsiOkved || this.props.nsiOkved.length === 0){
      view = <div>load...</div>;
    } else {

      if(this.state.treeNode) {
        this.state.treeNode.props.dataRef.children = this.props.tree ? this.props.tree : []
      }

      view = <Tree onExpand={this.onExpand} icon={false} loadData={this.onLoadData}>
        {this.renderTreeNodes(this.props.nsiOkved)}
      </Tree>;
    }

    return <Layout view={view} token={this.state.token} err={this.props.nsiOkvedError}/>
  }
}

const mapStateToProps = (state) => {
  return {
    tree: state.tree,
    nsiOkved: state.nsiOkved,
    nsiOkvedError: state.nsiOkvedError,
    gettingNsiOkved: state.gettingNsiOkved
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getNsiOkvedList: (data, treeNode = null) => dispatch(getNsiOkvedList(data, treeNode)),
    getNsiOkvedTree: (data) => dispatch(getNsiOkvedTree(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithToken(RegistrationCompany))
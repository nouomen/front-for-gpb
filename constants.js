/**
 * URL API
 * @type {string}
 */
// export const API_URL = 'http://gpb.loc/api';
export const API_URL = process.env.REACT_APP_API_URL;
export const PHONE_MASK = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

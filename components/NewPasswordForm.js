import React from 'react'
import PropTypes from 'prop-types'
import { Button, Icon, Input, Form, Row, Col } from 'antd'
import WithSpinner from '../hoc/WithSpinner'
import AlertError from './AlertError'

const NewPasswordForm = (props) => {
  return (
    <Row type="flex" justify="center" align="middle" style={{ height: '100%' }}>
      <Col>
        <AlertError err={props.newPasswordError}/>
        <h4>Восстановление пароля</h4>
        <Form layout="inline">
          <Form.Item>
            <Input.Password prefix={<Icon type="lock"/>} onChange={(e) => {props.updatePassword(e.target.value)}}
                            placeholder="Новый пароль"/>
          </Form.Item>
          <Form.Item>
            <Button onClick={() => props.onClick()} type="primary">Послать</Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  )
}

// TextFields.propTypes = {
//
// };

export default WithSpinner(NewPasswordForm)

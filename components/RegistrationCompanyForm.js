import React from 'react'
import PropTypes from 'prop-types'
import WithSpinner from '../hoc/WithSpinner'
import { Button, Col, Icon, Input, Row, Select, Tree  } from 'antd'
import MaskedPhone from '../components/Multipurpose/MaskedPhone'
import {PHONE_MASK} from '../constants'
import OkvedList from './Multipurpose/OkvedList'

const { Option } = Select

const RegistrationCompanyForm = ({treeData, onNsiOkvedChange}) => {
  return (
    <Row className="registration-company-form">
      <Col span={12} offset={6}>

        <Row type="flex" justify="center" align="middle">
          <Col>
            <h3>Регистрация компаний</h3>
          </Col>
        </Row>


        <Row type="flex" justify="center" align="middle">
          <Col>
            <OkvedList onNsiOkvedChange={onNsiOkvedChange} treeData={treeData} />
          </Col>
        </Row>

        <Row type="flex" justify="center" align="middle">
          <Col span={9}>
            <Select placeholder="Тип организации">
              <Option value="jack">Физическое лицо</Option>
              <Option value="lucy">Юридическое лицо</Option>
              <Option value="Yiminghe">Индивидуальный предприниматель</Option>
            </Select>
          </Col>
          <Col span={9}>
            <MaskedPhone mask={PHONE_MASK}/>
          </Col>
        </Row>

        <Row type="flex" justify="center" align="middle">
          <Col span={18}>
            <Input prefix={<Icon type="user"/>} id="full_name"
                   type="text" placeholder="Полное наименование организации"/>
          </Col>
        </Row>

        <Row type="flex" justify="center" align="middle">
          <Col span={18}>
            <Input prefix={<Icon type="user"/>} id="short_name"
                   type="text" placeholder="Краткое наименование организаци"/>
          </Col>
        </Row>

        <Row type="flex" justify="center" align="middle">
          <Col span={12}>
            <Input prefix={<Icon type="woman"/>} id="contact_person" type="text" placeholder="Контактное лицо"/>
          </Col>
          <Col span={6}>
            <Input prefix={<Icon type="mail"/>} id="email"
                   type="email" placeholder="E-mail"/>
          </Col>
        </Row>

        <Row className="inn-group" type="flex" justify="center" align="middle">
          <Col span={6}>
            <Input prefix={<Icon type="profile"/>} id="inn" type="text" placeholder="ИНН"/>
          </Col>

          <Col span={6}>
            <Input prefix={<Icon type="profile"/>} id="kpp" type="text" placeholder="КПП"/>
          </Col>

          <Col span={6}>
            <Input prefix={<Icon type="profile"/>} id="ogrn" type="text" placeholder="ОГРН"/>
          </Col>
        </Row>

        <Row type="flex" justify="space-around" align="middle">
          <Col span={18}>
            <Input prefix={<Icon type="environment"/>} id="address_postal" type="text" placeholder="Почтовый адрес"/>
          </Col>
        </Row>

        <Row type="flex" justify="space-around" align="middle">
          <Col span={18}>
            <Input prefix={<Icon type="environment"/>} id="address_legal" type="text" placeholder="Юридический адрес"/>
          </Col>
        </Row>

        <Row type="flex" justify="center" align="bottom">
          <Col>
            <Button style={{ 'width': '100%' }} type="primary">Регистрация</Button>
          </Col>

        </Row>

      </Col>
    </Row>
  )
}

// TextFields.propTypes = {
//
// };

export default WithSpinner(RegistrationCompanyForm)

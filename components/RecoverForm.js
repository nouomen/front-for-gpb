import React from 'react'
import PropTypes from 'prop-types'
import { Button, Icon, Input, Form } from 'antd'
import WithSpinner from '../hoc/WithSpinner'
import AlertError from './AlertError'

const RecoverForm = (props) => {
  return (
    <div>
      <AlertError err={props.recoverError}/>
      <h4>Восстановление пароля</h4>
      <Form layout="inline">
        <Form.Item>
          <Input prefix={<Icon type="mail" />} onChange={(e) => {props.updateEmail(e.target.value)}} placeholder="Логин или email"/>
        </Form.Item>
        <Form.Item>
          <Button onClick={() => props.onClick()}  type="button">Послать</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

// TextFields.propTypes = {
//
// };

export default WithSpinner(RecoverForm)

import React from 'react'
import { NavLink } from 'react-router-dom'
import { Menu, Icon } from 'antd'

const { SubMenu } = Menu

const Navigation = ({isAdmin}) => {
  return <Menu theme="dark" mode="horizontal" style={{ lineHeight: '64px' }}>

    <Menu.Item key="1">
      <NavLink exact to='/'>
        <Icon type="home"/> Главная
      </NavLink>
    </Menu.Item>

    <Menu.Item key="2">
      <NavLink to='/about'>
        <Icon type="global"/> О нас
      </NavLink>
    </Menu.Item>

    <Menu.Item key="3">
      <NavLink to='/login'>
        <Icon type="login"/> Авторизация
      </NavLink>
    </Menu.Item>

    <Menu.Item key="4">
      <NavLink to='/registration'>
        <Icon type="user-add"/> Регистрация
      </NavLink>
    </Menu.Item>

    <Menu.Item key="5">
      <NavLink to='/users'>
        <Icon type="team"/> Пользователи
      </NavLink>
    </Menu.Item>

    <SubMenu hidden={ !isAdmin } title={<span className="submenu-title-wrapper"><Icon type="solution"/>Организации</span>}>
      <Menu.Item key="6">
        <NavLink to='/company-registration'>Регистрация</NavLink>
      </Menu.Item>
      <Menu.Item key="7">
        <NavLink to='/company-search'>Поиск</NavLink>
      </Menu.Item>
    </SubMenu>

  </Menu>
}

export default Navigation
import React from 'react'
import { Button, Col, Row } from 'antd'
import cookie from 'react-cookies'
import jwt_decode from 'jwt-decode'

const LogoutForm = (...props) => {
  let token = cookie.load('token');
  let username = token ? jwt_decode(token).username : props.username

  return (
    <Row type="flex" justify="center">
      <Col>
        <div>Log out from {username}</div>
        <Button type="primary" onClick={ () => {
          cookie.remove('token')
          window.location.reload()
        }}>Выход</Button>
      </Col>
    </Row>
  )
}

export default LogoutForm
import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import WithSpinner from '../hoc/WithSpinner'
import AlertError from '../components/AlertError'
import { Col, Row, Button, Icon, Input, Form } from 'antd'

const LoginForm = (props) => {
  return (
    <Row type="flex" justify="center" align="middle" style={{ height: '100%' }}>
      <Col>
        <h3 style={{textAlign: 'center'}}>Вход</h3>
        <Form className="login-form">
          <AlertError err={props.jwtError}/>
          <Form.Item>
            <Input prefix={<Icon type="user"/>} onChange={(e) => {props.updateName(e.target.value)}} id="name"
                   type="email"
                   placeholder="Enter email"/>
          </Form.Item>
          <Form.Item>
            <Input.Password prefix={<Icon type="lock"/>} onChange={(e) => {props.updatePassword(e.target.value)}} id="password"
                   type="password" placeholder="Password"/>
          </Form.Item>
          <Form.Item>
            <Row type="flex" justify="end">
              <Col><Button type="primary" onClick={() => props.onClick()}>Вход</Button></Col>
            </Row>
            <Row type="flex" justify="space-between">
              <Col><NavLink to='/pass-recover'>Забыл пароль</NavLink></Col>
              <Col><NavLink to='/registration' >Регистрация</NavLink></Col>
            </Row>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  )
}

// TextFields.propTypes = {
//
// };

export default WithSpinner(LoginForm)

import React from 'react'
import { Icon, Input } from 'antd'
import MaskedInput from 'react-text-mask'

const MaskedPhone = ({ mask, updatePhone }) => {
  return (
    <span className="ant-input-affix-wrapper">
      <span className="ant-input-prefix">
        <Icon type="phone"/>
      </span>
      <MaskedInput
        onChange={(e) => {updatePhone(e.target.value)}}
        placeholder="Телефон"
        type="phone"
        id="phone"
        className="ant-input"
        mask={mask}/>
    </span>
  )
}

export default MaskedPhone
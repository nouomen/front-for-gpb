import React, { Component } from 'react'
import { Tree } from 'antd';
const { TreeNode } = Tree;

class OkvedList extends Component {

  constructor (props) {
    super(props);
  }

  onLoadData = treeNode => {
    this.props.onNsiOkvedChange(treeNode);
  }

  renderTreeNodes = data => {
    return data.map(item => {
      if (item.children) {
        return (
          <TreeNode title={item.title} key={item.key} dataRef={item}>
            {this.renderTreeNodes(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode isLeaf={false}  title={item.name} key={item.code} dataRef={item}/>;
    });
  }

  render() {
    return <Tree loadData={this.onLoadData}>
      {this.renderTreeNodes(this.props.treeData)}
    </Tree>;
  }
}


export default OkvedList
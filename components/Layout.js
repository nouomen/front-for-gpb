import React from 'react'
import Navigation from '../components/Navigation'
import LogoutForm from '../components/LogoutForm'
import { Layout } from 'antd'
import AlertError from './AlertError'

const { Header, Footer, Content, Sider } = Layout

export default ({view, err, token}) => {

  return (
    <div>
      <Layout className="layout">
        <Header>
          <div className="logo"/>
          <Navigation isAdmin={!!token}/>
        </Header>
        <Layout>
          <Content>
            <AlertError err={err}/>
            <div className="content">{view}</div>
          </Content>
          {token ?
            <Sider>
              <LogoutForm token={token}/>
            </Sider> :
            null}
        </Layout>
        <Footer>Goper ©2019 Created by Goper</Footer>
      </Layout>
    </div>
  )
}
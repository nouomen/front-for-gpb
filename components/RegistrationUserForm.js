import React from 'react'
import PropTypes from 'prop-types'
import WithSpinner from '../hoc/WithSpinner'
import { Button, Col, Icon, Input, Row, Form } from 'antd'
import { PHONE_MASK } from '../constants'
import MaskedPhone from './Multipurpose/MaskedPhone'

const RegistrationUserForm = (props) => {
  return (

    <Row type="flex" justify="center" align="middle" style={{ height: '100%' }}>
      <Col>
        <h3 style={{textAlign: 'center'}}>Регистрация пользователей</h3>
        <Form className="login-form">
          <Form.Item>
            <Input prefix={<Icon type="user"/>} onChange={(e) => {props.updateLogin(e.target.value)}} id="username"
                   type="text" placeholder="Логин"/>
          </Form.Item>
          <Form.Item>
            <Input prefix={<Icon type="mail"/>} onChange={(e) => {props.updateEmail(e.target.value)}} id="email"
                   type="email" placeholder="E-mail"/>
          </Form.Item>
          <Form.Item>
            <Input.Password prefix={<Icon type="lock"/>} id="password" type="password" placeholder="Пароль"
                   onChange={(e) => {props.updatePassword(e.target.value)}}/>
          </Form.Item>
          <Form.Item>
            <MaskedPhone mask={PHONE_MASK} updatePhone={props.updatePhone}/>
          </Form.Item>
          <Form.Item>
            <Button onClick={(e) => props.onClick(e)} type="primary">Регистрация</Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  )
}

// TextFields.propTypes = {
//
// };

export default WithSpinner(RegistrationUserForm)

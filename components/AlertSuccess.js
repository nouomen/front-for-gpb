import React from 'react'
import { Alert, Col, Row } from 'antd'

const AlertSuccess = (props) => {
  return (
    <Row type="flex" justify="center" align="middle" style={{ height: '100%' }}>
      <Col>
        <Alert message="Success" showIcon description={props.message} type="success" closable>
        </Alert>
      </Col>
    </Row>
  )

}

export default AlertSuccess
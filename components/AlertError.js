import React from 'react'
import { Alert } from 'antd'
const AlertError = (props) => props.err ? <Alert message="Error" showIcon  description={props.err} type="error" closable/> : null
export default AlertError
import axi from 'axios'
import * as constants from '../constants'
import cookie from 'react-cookies'

export function usersError (desc) {
  return {
    type: 'USERS_ERROR',
    error: desc
  }
}

export function gettingUsers (bool) {
  return {
    type: 'GETTING_USERS',
    gettingUsers: bool
  }
}

export function users (users) {
  return {
    type: 'USERS',
    users
  }
}

export function getUsers (data) {
  const { API_URL } = constants

  return (dispatch) => {
    (async () => {
      try {
        dispatch(usersError(null))
        dispatch(gettingUsers(true));
        const headers = {
          'Authorization': `Bearer ${cookie.load('token')}`
        }
        let response = await axi.post(API_URL + '/user/list', JSON.stringify(data), {headers: headers})
        dispatch(gettingUsers(false))
        dispatch(users(response.data))
      } catch (e) {
        dispatch(gettingUsers(false))
        dispatch(usersError(e.response.data))
      }
    })()
  }
}
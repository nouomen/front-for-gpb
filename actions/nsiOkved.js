import axi from 'axios'
import * as constants from '../constants'
import cookie from 'react-cookies'

const headers = {
  'Authorization': `Bearer ${cookie.load('token')}`
}

const { API_URL } = constants

export function nsiOkvedError (desc) {
  return {
    type: 'NSI_OKVED_ERROR',
    error: desc
  }
}

export function gettingNsiOkved (bool) {
  return {
    type: 'GETTING_NSI_OKVED',
    gettingNsiOkved: bool
  }
}

export function nsiOkved (nsiOkvedList) {
  return {
    type: 'NSI_OKVED',
    nsiOkvedList: nsiOkvedList
  }
}

export function tree(tree) {
  return {
    type: 'NSI_TREE',
    tree: tree
  }
}

export function getNsiOkvedTree ({treeNode}) {

  let data = { 'code': treeNode.props.eventKey }
  let promise = axi.post(API_URL + '/nsi/okved/list', JSON.stringify(data), {headers: headers})

  // promise.then((response) => {
  //
  //   if (treeNode.props.children) {
  //     return;
  //   }
  //
  //   treeNode.props.dataRef.children = response.data.rows
  //
  //   debugger
  //   // this.setState({
  //   //   treeData: [...this.state.treeData],
  //   // });
  //
  // }).catch((e) => {
  //
  //   return (dispatch) => dispatch(nsiOkvedError(e.response.data))
  //
  // });

  return (dispatch) => dispatch(tree(promise))

}

export function getNsiOkvedList (data, treeNode) {
  return (dispatch) => {
    (async () => {
      try {
        dispatch(nsiOkvedError(null))
        dispatch(gettingNsiOkved(true));
        let response = await axi.post(API_URL + '/nsi/okved/list', JSON.stringify(data), {headers: headers})
        dispatch(gettingNsiOkved(false))

        if(treeNode) {
          dispatch(tree(response.data.rows))
        } else{
          dispatch(nsiOkved(response.data.rows))
        }



      } catch (e) {
        console.log(e);
        dispatch(gettingNsiOkved(false))
        dispatch(nsiOkvedError(e.response.data))
      }
    })()
  }
}
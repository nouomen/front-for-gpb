import axi from 'axios'
import * as constants from '../constants'

export function newPasswordError (desc) {
  return {
    type: 'NEW_PASSWORD_ERROR',
    error: desc
  }
}

export function gettingNewPassword (bool) {
  return {
    type: 'GETTING_NEW_PASSWORD',
    gettingNewPassword: bool
  }
}

export function newPassword (result) {
  return {
    type: 'NEW_PASSWORD',
    result
  }
}

export function onNewPassword (data) {
  const { API_URL } = constants

  return (dispatch) => {
    (async () => {
      try {
        dispatch(newPasswordError(null))
        dispatch(gettingNewPassword(true));
        let response = await axi.post(API_URL + '/auth/changepassword', JSON.stringify(data))
        dispatch(gettingNewPassword(false))
        response.data.success === true ? dispatch(newPassword(response.data.success)) : dispatch(newPasswordError(response.data.message))
      } catch (e) {
        dispatch(gettingNewPassword(false))
        console.log(e)
        alert(e)
      }
    })()
  }
}
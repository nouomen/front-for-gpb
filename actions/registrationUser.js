import axi from 'axios'
import * as constants from '../constants'

export function registrationUserError (desc) {
  return {
    type: 'REGISTRATION_USER_ERROR',
    registrationUserError: desc
  }
}

export function gettingRegistrationUser (bool) {
  return {
    type: 'GETTING_REGISTRATION_USER',
    gettingRegistrationUser: bool
  }
}

export function registrationUser (registrationUserResponse) {
  return {
    type: 'REGISTRATION_USER',
    registrationUserResponse
  }
}

export function makeRegistrationUser (data) {
  const { API_URL } = constants

  return (dispatch) => {
    (async () => {
      try {
        dispatch(gettingRegistrationUser(true))
        dispatch(registrationUserError(null))
        let response = await axi.post(API_URL + '/auth/register', JSON.stringify(data))
        
        dispatch(gettingRegistrationUser(false))

        if(response.data.success === true) {
          dispatch(registrationUser(true))
        } else{
          dispatch(registrationUserError(response.data.message))
        }

      } catch (e) {
        dispatch(gettingRegistrationUser(false))
        dispatch(registrationUserError(e.response.data))
      }
    })()
  }
}
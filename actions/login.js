import axi from 'axios'
import * as constants from '../constants'
import cookie from 'react-cookies'
import jwt_decode from 'jwt-decode'

export function jwtError (desc) {
  return {
    type: 'JWT_ERROR',
    error: desc
  }
}

export function gettingJwt (bool) {
  return {
    type: 'GETTING_JWT',
    gettingJwt: bool
  }
}

export function jwt (token) {
  return {
    type: 'JWT',
    token
  }
}

export function getJwt (data) {
  const { API_URL } = constants

  return (dispatch) => {
    (async () => {
      try {
        dispatch(gettingJwt(true));
        dispatch(jwtError(null))
        let response = await axi.post(API_URL + '/auth/login', JSON.stringify(data))
        dispatch(gettingJwt(false))
        dispatch(jwt(response.data))
        let exp = jwt_decode(response.data.token).exp
        cookie.save('token', response.data.token, { path: '/', expires: new Date(exp * 1000)})
      } catch (e) {
        dispatch(gettingJwt(false))
        dispatch(jwtError(e.response.data))
      }
    })()
  }
}
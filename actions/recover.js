import axi from 'axios'
import * as constants from '../constants'

export function recoverError (desc) {
  return {
    type: 'RECOVER_ERROR',
    error: desc
  }
}

export function gettingLink (bool) {
  return {
    type: 'GETTING_LINK',
    gettingLink: bool
  }
}

export function link (answer) {
  return {
    type: 'LINK',
    answer
  }
}

export function getRecoverLink (data) {
  const { API_URL } = constants

  return (dispatch) => {
    (async () => {
      try {
        dispatch(recoverError(null))
        dispatch(gettingLink(true))
        let response = await axi.post(API_URL + '/auth/resettoemail', JSON.stringify(data))
        dispatch(gettingLink(false))
        response.data.success === true ? dispatch(link(response.data.confirmation_token)) : dispatch(recoverError(response.data.message))
      } catch (e) {
        console.log(e)
        alert(e)
        dispatch(recoverError(e))
        dispatch(gettingLink(false))
      }
    })()
  }
}
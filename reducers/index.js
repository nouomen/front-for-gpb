import { combineReducers } from 'redux'
import { jwt, jwtError, gettingJwt } from './login'
import { registrationUser, registrationUserError, gettingRegistrationUser } from './registrationUser'
import { link, recoverError, gettingLink } from './recover'
import { newPasswordError, gettingNewPassword, newPassword } from './newPassword'
import { usersError, gettingUsers, users } from './users'
import { nsiOkvedError, gettingNsiOkved, nsiOkved, tree } from './nsiOkved'

// соединяет редьюсеры
export default combineReducers({
  // авторизация
  jwtError,
  gettingJwt,
  jwt,
  // регистрация
  registrationUserError,
   gettingRegistrationUser,
   registrationUser,
  // восстановление
  link,
  recoverError,
  gettingLink,
  // получение нового пароля
  newPasswordError,
  gettingNewPassword,
  newPassword,
  // получение юзеров
  usersError,
  gettingUsers,
  users,
  // вывод справочника оквед
  nsiOkvedError,
  gettingNsiOkved,
  nsiOkved,
  tree
})
// это чистая функция, которая меняет store.
export function recoverError (state = null, action) {

  return action.type === 'RECOVER_ERROR' ? action.error : state

}

export function gettingLink (state = null, action) {

  return action.type === 'GETTING_LINK' ? action.gettingLink : state

}

export function link (state = null, action) {

  return action.type === 'LINK' ? action.answer : state

}
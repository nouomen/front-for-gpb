// это чистая функция, которая меняет store.
export function registrationUserError (state = null, action) {

  return action.type === 'REGISTRATION_USER_ERROR' ? action.registrationUserError : state

}

export function gettingRegistrationUser(state = null, action) {

  return action.type === 'GETTING_REGISTRATION_USER' ? action.gettingRegistrationUser : state

}

export function registrationUser (state = null, action) {

  return action.type === 'REGISTRATION_USER' ? action.registrationUserResponse : state

}
// это чистая функция, которая меняет store.
export function nsiOkvedError (state = null, action) {

  return action.type === 'NSI_OKVED_ERROR' ? action.error : state

}

export function gettingNsiOkved (state = null, action) {

  return action.type === 'GETTING_NSI_OKVED' ? action.gettingNsiOkved : state

}

export function nsiOkved (state = null, action) {

    return action.type === 'NSI_OKVED' ?  action.nsiOkvedList : state
}


export function tree (state = null, action) {

  if(action.type === 'NSI_TREE') {
      // старое дерево из state +  новое дерево из action.payload.tree
      return action.tree

  }

  return state

}
// это чистая функция, которая меняет store.
export function usersError (state = null, action) {

  return action.type === 'USERS_ERROR' ? action.error : state

}

export function gettingUsers (state = null, action) {

  return action.type === 'GETTING_USERS' ? action.gettingUsers : state

}

export function users (state = null, action) {

  return action.type === 'USERS' ? action.users : state

}
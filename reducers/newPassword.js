// это чистая функция, которая меняет store.
export function newPasswordError (state = null, action) {

  return action.type === 'NEW_PASSWORD_ERROR' ? action.error : state

}

export function gettingNewPassword (state = null, action) {

  return action.type === 'GETTING_NEW_PASSWORD' ? action.gettingNewPassword : state

}

export function newPassword (state = null, action) {

  return action.type === 'NEW_PASSWORD' ? action.result : state

}
// это чистая функция, которая меняет store.
export function jwtError (state = null, action) {

  return action.type === 'JWT_ERROR' ? action.error : state

}

export function gettingJwt (state = null, action) {

  return action.type === 'GETTING_JWT' ? action.gettingJwt : state

}

export function jwt (state = null, action) {

  return action.type === 'JWT' ? action.token : state

}
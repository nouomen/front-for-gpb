import React from 'react'
import ReactDOM from 'react-dom'
import 'antd/dist/antd.css';
import './index.css'
import './App.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import reducer from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import About from './components/About'
import Login from './containers/Login'
import Registration from './containers/RegistrationUser'
import Recover from './containers/Recover'
import NewPassword from './containers/NewPassword'
import Users from './containers/Users'
import RegistrationCompany from './containers/RegistrationCompany'

// 2й параметр нужен для работы плагина в chrome
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path="/" exact render={props => <App/>}/>
        <Route path="/about" render={props => <About {...props} />}/>
        <Route path="/login" render={props => <Login {...props} />}/>
        <Route path="/registration" render={props => <Registration {...props} />}/>
        <Route path="/pass-recover" render={props => <Recover {...props} />}/>
        <Route path="/new-password/:hash" render={props => <NewPassword {...props} />}/>
        <Route path="/users" render={props => <Users {...props} />}/>
        <Route path="/company-registration" render={props => <RegistrationCompany {...props} />}/>
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
